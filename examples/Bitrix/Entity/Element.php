<?php
//компилим сущность для нужного инфоблока
/** @var \Bitrix\Main\Entity\DataManager $entity */
$entity = \SB\Bitrix\Entity\Element::compileEntityByIBlock(1);

//выбираем необходимое
$arElements = $entity::getList([
    'filter' => [
        'PROPERTY_TEST.VALUE' => "qwe"
    ],
    'select' => [
        'ID',
        'PROPERTY_TEST.VALUE_ARRAY'
    ]
])->fetchAll();