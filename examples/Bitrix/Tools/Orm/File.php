<?php
# BITRIX CORE
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Entity\Query;

$iblockId = 5;

try {
    $query = (new Query(ElementTable::getEntity()))
        ->setFilter([
            '=IBLOCK_ID' => $iblockId,
            '=ID' => '100700'
        ])
        ->setSelect(['ID', 'NAME', 'SMALL_IMAGE']);

    $query = \SB\Bitrix\Tools\Orm\File::attachToQuery($query, 'SMALL_IMAGE', 'PREVIEW_PICTURE');

    $result = $query->exec();

    _::d($result->fetchAll());

} catch (Throwable $throwable) {
    _::dd($throwable);
}