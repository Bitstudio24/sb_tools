<?php

use Bitrix\Main\IO;
use SB\File\Csv\Reader;

try {
    $csvFilePath = __DIR__ . '/TemplateFile.csv';

    # проверяем сущестрование файла
    if (!file_exists($csvFilePath)) {
        throw new IO\FileNotFoundException($csvFilePath);
    }

    # открываем исходный файл и блокируем
    $csvFile = fopen($csvFilePath, 'rb');
    if (!$csvFile) {
        throw new IO\FileOpenException($csvFilePath);
    }
    flock($csvFile, LOCK_EX);

    # создаем Reader
    $reader = new Reader($csvFile);
    $reader->setOption(Reader::OPTION_FIRST_LINE_HEADER, true);

    # читаем файл в массив
    $result = [];

    foreach ($reader->read() as $item) {
        $result[] = $item;
    }

    flock($csvFile, LOCK_NB);
    fclose($csvFile);
} catch (\Throwable $throwable) {
    die($throwable->getMessage());
}