<?php

use SB\Traits\Timing;

/** @noinspection AutoloadingIssuesInspection */
class TimingExample
{
    use Timing;

    public function setTimer(int $time = 1): string
    {
        $this::startTimer();

        if ($time > 5) {
            $time = 5;
        }

        sleep($time);

        return $this::getTiming();
    }
}

$timingExample = new TimingExample();

echo $timingExample->setTimer(3);