<?php

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Loader;
use SB\Model\Manager\CityPool;

class Domain extends \SB\Manager\Domain
{
    /**
     * загрузка списка городов
     * @return CityPool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \SB\Exception\LogicException
     */
    protected function loadCityList(): CityPool
    {
        $cityPool = new CityPool();
        Loader::includeModule('iblock');
        $dbElements = ElementTable::getList([
            'filter' => [
                'IBLOCK_ID' => 2,
                'ACTIVE' => 'Y'
            ],
            'select' => ['ID', 'NAME', 'CODE']
        ]);
        while ($arElement = $dbElements->fetch()) {
            $default = (int) $arElement['ID'] === 2;
            $cityPool->add($arElement['CODE'], $arElement['NAME'], $default);
        }

        return $cityPool;
    }
}