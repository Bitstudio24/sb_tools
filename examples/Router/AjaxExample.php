<?php
namespace SB\Site\Handler\Ajax;

use Bitrix\Main\HttpRequest;
use SB\Handler\Ajax;

class MyClass extends Ajax
{
    public function myFunction()
    {
        /** добавление ошибки */
        $this->getResult()->addError('test', 1);
        /** установка статуса ответа */
        $this->getResult()->setStatus(true);
        /** добавление результата */
        $this->getResult()->addData('dataKey', 'dataValue');

        /** @var HttpRequest $request получение запроса */
        $request = $this->getRequest();

        /** @var array $params получение (get/post/cookie) */
        $params = $this->getParams();
    }
}