<?php
//инициализация
$manager = \SB\Site\Manager\Domain::getInstance();
/** @var \SB\Model\Manager\City $city текущей установленный город */
$city = $manager->getCity();
/** @var \SB\Model\Manager\CityPool $cityList список городов */
$cityList = $manager->getCityList();

// получить город по коду
/** @var \SB\Model\Manager\City $city */
$city = $cityList->getByCode('cityCode');

// перейти на другой город
$manager->goToCity($city);
$manager->goToCityByCode('cityCode');