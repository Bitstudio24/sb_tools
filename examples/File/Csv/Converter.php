<?php

use Bitrix\Main\IO;
use SB\File\Csv\Converter;
use SB\File\Csv\Reader;
use SB\File\Csv\Writer;

/** @noinspection AutoloadingIssuesInspection */
class ConverterExample
{
    /**
     * @param string $baseFilePath
     * @param string $resultFilePath
     * @throws IO\FileNotFoundException
     * @throws IO\FileOpenException
     */
    public function convert(string $baseFilePath, string $resultFilePath)
    {
        # проверяем сущестрование файлов
        if (!file_exists($baseFilePath) || !is_file($baseFilePath)) {
            throw new IO\FileNotFoundException($baseFilePath);
        }

        # открываем исходный файл и блокируем
        $baseFile = fopen($baseFilePath, 'rb');
        if (!$baseFile) {
            throw new IO\FileOpenException($baseFilePath);
        }
        flock($baseFile, LOCK_EX);

        # открываем файл результата и блокируем
        $resultFile = fopen($resultFilePath, 'wb+');
        if (!$resultFile) {
            throw new IO\FileOpenException($resultFilePath);
        }
        flock($resultFile, LOCK_EX);

        /**
         * @var array $convertMap
         * массив соответствия столбцов, может формироваться динамически
         * ключи - столбцы текущего массива
         * значения - столбцы результирующего массива, порядок столбцов будет сохранен
         */
        $convertMap = [
            'ID' => 'REWRITE_ID',
            'XML_ID' => 'REWRITE_XML_ID',
            'CODE' => 'REWRITE_CODE',
            'ACTIVE' => 'REWRITE_ACTIVE',
            'NAME' => 'REWRITE_NAME',
        ];

        # создаем Reader
        $reader = new Reader($baseFile);
        $reader->setOption(Reader::OPTION_FIRST_LINE_HEADER, true);

        # создаем Writer
        $writer = new Writer($resultFile);
        $writer
            ->setColumns(array_values($convertMap))
            ->setOption(Writer::OPTION_FIRST_LINE_HEADER, true);

        # создаем Converter
        $converter = new Converter(
            $reader,
            $writer,
            $convertMap,
            function ($incoming, &$out) use ($convertMap) {
                if (!empty($convertMap['CODE'])) {
                    $out[$convertMap['CODE']] = static::getCode($incoming['CODE']);
                }
                return $out;
            }
        );

        echo $converter->process();

        flock($baseFile, LOCK_NB);
        flock($resultFile, LOCK_NB);
        fclose($baseFile);
        fclose($resultFile);
    }

    /**
     * Пример функции для модификации данных
     * @param $value
     * @return string
     */
    public static function getCode($value): string
    {
        return $value . '_MODIFY';
    }
}

try {
    $oldCsvFilePath = __DIR__ . '/TemplateFile.csv';
    $newCsvFilePath = __DIR__ . '/TemplateFileRewrite.csv';

    $converter = new ConverterExample();
    $converter->convert($oldCsvFilePath, $newCsvFilePath);
} catch (\Throwable $throwable) {
    die($throwable->getMessage());
}