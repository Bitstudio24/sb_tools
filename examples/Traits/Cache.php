<?php
/** выполнение функции без кэша */
$result = Test::myFunc();
/** выполнение статической функции с кэшем */
$result = Test::myFuncCache();
$obj = new Test();
/** выполнение динамической функции с кэшем */
$result = $obj->myFunCache();

/** удалить кэш всего класса */
Test::getTraitCache()->cleanCache();
/** удалить кэш всей функции */
Test::getTraitCache()->cleanFuncCache('myFunc');
/** удалить кэш функции с определенными параметрами ('test', 123) */
Test::getTraitCache()->cleanFuncArgsCache('myFunc', ['test', 123]);
