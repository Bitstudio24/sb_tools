<?php

class SimpleReadWriteCsv
{
    public static function read($fileName, $separator = ",")
    {
        $handle = fopen($fileName, "rb"); //Открываем csv для чтения
        $array_line_full = array(); //Массив будет хранить данные из csv
        while (($line = fgetcsv($handle, 0, $separator)) !== false) { //Проходим весь csv-файл, и читаем построчно. 3-ий параметр разделитель поля
            $array_line_full[] = $line; //Записываем строчки в массив
        }
        fclose($handle); //Закрываем файл
        return $array_line_full; //Возвращаем прочтенные данные
    }

    public static function write($rewriteName, array $csv, $separator = ";")
    {
        $handle = fopen($rewriteName, "ab"); //Открываем csv для до-записи, если указать w, то  ифномация которая была в csv будет затерта
        fputcsv($handle, $csv, $separator); //Записываем, 3-ий параметр - разделитель поля
        fclose($handle); //Закрываем
    }
}
