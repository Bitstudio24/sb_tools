<?php
use Bitrix\Main\EventResult;
//выполнение функции myFuncAgent с параметрами param1, params2
$agent = new \SB\Handler\Agent('myFuncAgent', ['param1', 'param2']);
$agent->setLogClass(\SB\Tools\Output\FileOutput::class)
    ->setIsUnique(true) //создаёт только одного агента с подобными параметрами
    ->setInterval(5)
    ->setNextExec(new \Bitrix\Main\Type\DateTime('15.03.2018', 'd.m.Y'))
    ->setLogArgs([$_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/logs/test.txt']);
$agent->create();

function myFuncAgent(\Bitrix\Main\Event $event) {
    // параметры функции param1, param2
    $params = $event->getParameter('args');
    /** @var \SB\Tools\Output $log */
    $log = $event->getParameter('log');
    $log->writeln('test');

    //изменение параметров
    $event->addResult(new EventResult(EventResult::SUCCESS, ['args' => ['param2', 'param3']]));
    //прекращение выполнения агента
    $event->addResult(new EventResult(EventResult::ERROR));
}