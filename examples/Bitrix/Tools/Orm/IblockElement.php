<?php
# BITRIX CORE
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

use SB\Bitrix\Tools\Orm\IblockElement;

$iblockId = 5;

try {
    $query = IblockElement::getIBlockElementQuery($iblockId,
        IblockElement::REF_ELEMENT_PROPERTY | IblockElement::REF_ELEMENT_UF_SECTION);

    $query->addFilter('ID', '100700');
    $query->setSelect(['ID', 'PROPERTY.COLOR', 'PROPERTY.PRODUCER', 'SECTION_REF.NAME']);

    $result = $query->exec();

    _::d($result->fetchAll());

} catch (Throwable $throwable) {
    _::dd($throwable);
}