<?php

use SB\Traits;

/**
 * Class Test
 * @package SB\Site
 * @method static myFuncCache($param = null)
 * @method myFunCache($param)
 */
class Test
{
    use Traits\Cache;

    /**
     * @cache_time 300
     * @cache_addition getAddition
     */
    public static function myFunc($param = null)
    {
        $time = time();
        return $time . $param . 'staticNew';
    }


    public function myFun($param)
    {
        return 'dinamic';
    }

    public static function getAddition($functionName, $args)
    {
        return 'additionParam';
    }
}