<?php
# BITRIX CORE
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionElementTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Main\Entity\ReferenceField;

$iblockId = 5;
$sectionEntity = \SB\Bitrix\Tools\Orm\IblockSection::getIBlockSectionEntity($iblockId);

/**
 * Задача - получить список ИД элементов инфоблока, в разделе которого
 * стоит пользовательское свойство "Выгружать в фид яндекс маркета" (Да\Нет)
 */

/**
 * Вариант 1:
 */
try {
    # Этап 1: Получение разделов инфоблока, у которых свойство IF_IS_IMPORT равно 1
    $query = (new Query($sectionEntity))
        ->setFilter([
            '=IBLOCK_ID' => $iblockId,
            '=IF_IS_IMPORT' => 1
        ])
        ->setSelect(['ID', 'NAME'])
        ->exec();

    $sectionList = $query->fetchAll();
    _::d($sectionList);

    $sectionIdList = array_column($sectionList, 'ID');

    # Этап 2: Получение товаров из этих разделов
    $query = (new Query(ElementTable::getEntity()))
        ->setFilter([
            '=IBLOCK_ID' => $iblockId,
            '@SECTION.ID' => $sectionIdList
        ])
        ->setSelect([
            'ID',
            'CODE',
            'NAME',
            'SORT',
            'SECTION_ID' => 'SECTION.ID',
            'SECTION_CODE' => 'SECTION.CODE',
            'SECTION_NAME' => 'SECTION.NAME'
        ])
        ->registerRuntimeField('',
            new ReferenceField(
                'SECTION',
                '\Bitrix\Iblock\Section',
                ['=this.IBLOCK_SECTION_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ))
        ->exec();

    $elementList = $query->fetchAll();
    _::d($elementList);

    # Итог: 2 раздела и 190 элементов

} catch (Throwable $throwable) {
    _::dd($throwable);
}

/**
 * Вариант 2: Получение ИД элементов инфоблока, привязанных к разделу,
 * у которого свойство IF_IS_IMPORT = 1 через таблицу SectionElementTable
 */
try {
    $query = (new Query(SectionElementTable::getEntity()))
        ->setFilter([
            '=SECTIONS.IBLOCK_ID' => $iblockId,
            '=SECTIONS.IF_IS_IMPORT' => 1,
        ])
        ->registerRuntimeField('', new ReferenceField(
            'SECTIONS',
            $sectionEntity,
            ['=this.IBLOCK_SECTION_ID' => 'ref.ID'],
            ['join_type' => 'LEFT']
        ))
        ->setGroup(['IBLOCK_ELEMENT_ID'])
        ->setSelect(['PRODUCT_ID' => 'IBLOCK_ELEMENT_ID', 'SECTIONS.ID'])
        ->exec();

    $productIdList = [];
    while ($product = $query->fetch()) {
        $productIdList[] = $product['PRODUCT_ID'];
    }

    _::d($productIdList);

    # Итог: 2 раздела и 194 элемента, так как некоторые товары были привязаны к 2 разделам
} catch (Throwable $throwable) {
    _::dd($throwable);
}

/**
 * Вариант 3: Получение элементов инфоблока, у которых свойство IF_IS_IMPORT равно 1,
 * запрос из таблицы ElementTable но с подключенной SectionElement для выборки всех разделов элемента
 */
try {
    $query = (new Query(ElementTable::getEntity()))
        ->setFilter([
            '=IBLOCK_ID' => $iblockId,
            '=SECTION.IF_IS_IMPORT' => 1
        ])
        ->setGroup(['ID'])
        ->setSelect([
            'ID',
            'CODE',
            'NAME',
            'SORT',
            'SECTION_ID' => 'SECTION.ID',
            'SECTION_CODE' => 'SECTION.CODE',
            'SECTION_NAME' => 'SECTION.NAME'
        ])
        ->registerRuntimeField('',
            new ReferenceField(
                'SECTIONS',
                'Bitrix\Iblock\SectionElement',
                ['=this.ID' => 'ref.IBLOCK_ELEMENT_ID']
            ))
        ->registerRuntimeField('',
            new ReferenceField(
                'SECTION',
                $sectionEntity,
                ['=this.SECTIONS.IBLOCK_SECTION_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ))
        ->exec();

    $elementList = $query->fetchAll();
    _::d($elementList);

    # Итог: 2 раздела и 194 элемента, так как некоторые товары были привязаны к 2 разделам
} catch (Throwable $throwable) {
    _::dd($throwable);
}
