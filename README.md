# SB_TOOLS
> Библиотека скриптов разной степени полезности

## Начало работы

1. Подключить к проекту
    ```
    git submodule add git@bitbucket.org:Bitstudio24/sb_tools.git local/php_interface/sb_tools/ && composer install --working-dir=$PWD/local/php_interface/sb_tools
    ```
2. Инициализировать
    ```php
    include_once 'sb_tools/init.php';
    ```
## Features 

- [Трейт для реализации кэширования методов](classes/Traits/Cache.php)
- [Роутер для Ajax запросов](classes/Handler/Ajax.php)

## Дополнительная информация

[Документация](http://docs.bitrx24.com/namespaces/SB.html)
 
[Полезные файлы с кодом](template/README.md)
    
[Примеры использования скриптов библиотеки](examples/README.md)

## COMING SOON / ISSUES

- CONSOLE APPLICATION
- MONOLOG
- ADMIN MENU EDITOR
- CONFIG APP
- USER FIELDS ENTITY
